function map(func, array)
  local new_array = {}
  for i,v in ipairs(array) do
    new_array[i] = func(v)
  end
  return new_array
end

function altLanguages(ls,l,href,indent)
   s = ""
   first = true
   for i,v in ipairs(ls) do
      if v[1] ~= l then
         if first then
            first = false
         else
            s = s.."\n"..indent
         end
         s = s.."link.altLanguage(rel='alternate', hreflang='"..v[1].."', href='"..href.."?Set-Language="..v[1].."')"
      end
   end
   return s
end

function gitFileTs(fn,rev)
   if rev == nil then
      rev = "HEAD"
   end
   local h = io.popen([[git show --pretty=format:%at --abbrev-commit `git rev-list "]]..rev..[[" "]]..fn..[["`|head -1]])
   local r = h:read("*a")
   h:close()
   local ts = r:gsub("\n$", "")
   return ts
end

function abspath(path)
   if path:match("^/") == nil then
      local curdir = os.getenv('PWD')
      return curdir .. "/" .. path
   else
      return path
   end
end

if configlua_filename ~= nil then
   configlua_filename = abspath(configlua_filename)
end
